/*
 * zad2.c
 *
 *  Created on: Nov 23, 2020
 *      Author: student
*/
#include <stdio.h>
#include <stdlib.h>


int main(void) {
	int a = 17;
	char b = 'b';
	unsigned char c = 'c';
	float d = 8742;
	double e = 3649;
	printf("Zmienna typu int a = %d, a jej wielkosc to %ld\n", a, sizeof(a));
	printf("Zmienna typu int b = %c, a jej wielkosc to %ld\n", b, sizeof(b));
	printf("Zmienna typu int c = %c, a jej wielkosc to %ld\n", c, sizeof(c));
	printf("Zmienna typu int d = %f, a jej wielkosc to %ld\n", d, sizeof(d));
	printf("Zmienna typu int e = %f, a jej wielkosc to %ld\n", e, sizeof(e));

	const int x = 7;
	int *adr = &x;
	*adr = 15;
	printf("zadeklarowano 10 a jest %d\n",x);

	return EXIT_SUCCESS;
}


