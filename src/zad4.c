/*
 * zad4.c
 *
 *  Created on: Nov 24, 2020
 *      Author: student
 */
#include <stdio.h>
#include <stdlib.h>


int main(void) {
	int a = 100;
	char b = 'b';
	unsigned char c = 'c';
	int *w1 =  &a;
	char *w2 = &b;
	unsigned char *w3 = &c;

	printf("rozmiar a = %ld, rozmiar wskaznika = %ld, wartosc a = %d\n",sizeof(a),sizeof(w1),a);
	printf("rozmiar b = %ld, rozmiar wskaznika = %ld, wartosc b = %c\n",sizeof(b),sizeof(w2),b);
	printf("rozmiar c = %ld, rozmiar wskaznika = %ld, wartosc c = %c\n",sizeof(c),sizeof(w3),c);

	*w1 = 2;
	*w2 = c;
	*w3 = b;

	printf("rozmiar a = %ld, rozmiar wskaznika = %ld, wartosc a = %d\n",sizeof(a),sizeof(w1),a);
	printf("rozmiar b = %ld, rozmiar wskaznika = %ld, wartosc b = %c\n",sizeof(b),sizeof(w2),b);
	printf("rozmiar c = %ld, rozmiar wskaznika = %ld, wartosc c = %c\n",sizeof(c),sizeof(w3),c);


return EXIT_SUCCESS;
}



